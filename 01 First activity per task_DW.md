-- First activity for a particular task
USE [phoenixdw.us]
GO

--DROP VIEW VIEWS.vw_AS_Conversion_01_FirstActivity 
--GO

ALTER VIEW VIEWS.vw_AS_Conversion_01_FirstActivity AS

SELECT
	TaskID,
	EntityIDTypeID,
	TaskDate,
	ActivityDate AS PrimaryActivityDate,
	ActivityGroup AS PrimaryActivityGroup,
	isInboundCallActivity,
	Activities,
	BrandName,
	ProductTypeGroup,
	MediaChannelID,
	VendorName,
	SpotLength,
	SourceID
FROM
(
	SELECT
		A.*,
		EntityIDTypeID,
		CONVERT(DATE, T.dtmCreated AT TIME ZONE 'UTC' AT TIME ZONE 'Pacific Standard Time') AS TaskDate,
		CONVERT(DATE, A.dtmCreated AT TIME ZONE 'UTC' AT TIME ZONE 'Pacific Standard Time') AS ActivityDate,
		ROW_NUMBER() OVER(PARTITION BY T.TaskID ORDER BY A.ActivityID) AS ActivityOrder,
		COUNT(*) OVER(PARTITION BY T.TaskID ) AS Activities,
		CASE 
			WHEN RT.TaskID IS NOT NULL THEN RT.ActivityGroup
			WHEN A.ActivityTypeID = 1 AND S.MediaChannelID IN (1) THEN 'Inbound - TV'
			--WHEN A.ActivityTypeID = 1 AND S.MediaChannelID IN (3, 16, 17, 18) THEN 'Inbound - Web'
			WHEN A.ActivityTypeID = 1 AND S.MediaChannelID IN (3, 16, 17, 18, 8, 13, 25) THEN 'Inbound - Other' -- Web, DM, Unkown, Sales (presentation numbers)
			WHEN A.ActivityTypeID = 1 AND S.MediaChannelID IN (39, 40, 41, 42) THEN 'Inbound - Call Transfers'
			WHEN A.ActivityTypeID = 2 THEN 'In hours messages'
			WHEN A.ActivityTypeID IN (6, 8) THEN 'Data leads' -- third party live competed / not-competed
			WHEN A.ActivityTypeID = 7 THEN 'Web leads'
			WHEN A.ActivityTypeID IN (17, 18, 49) THEN 'DM'
			--WHEN A.ActivityTypeID IN (27) THEN '3rd party outbound leads' -- outbound attempts
			WHEN A.ActivityTypeID IN (27) THEN S.MktingCampaignName
			WHEN A.ActivityTypeID = 28 THEN 'Inbound retention call'
			WHEN A.ActivityTypeID = 42 THEN 'Out of hours messages'
			WHEN A.ActivityTypeID = 47 THEN 'Client search'
			WHEN A.ActivityTypeID = 48 THEN 'Abandoned calls'
			WHEN S.MktingCampaignName LIKE ('%Reharvest%') THEN S.MktingCampaignName
		END AS ActivityGroup,
		CASE 
			WHEN S.MktingCampaignName LIKE ('%Reharvest%') THEN 0
			WHEN RT.TaskID IS NOT NULL THEN RT.isInboundCallActivity
			ELSE ATT.isInboundCallActivity END AS isInboundCallActivity,
		B.BrandName,
		PTG.ProductTypeGroup,
		S.MediaChannelID,
		S.SourceID,
		COALESCE(SV.VendorName , 'N/A') AS VendorName,
		COALESCE(CAST(SV.Length AS varchar), 'N/A') AS SpotLength


		
	FROM [phoenixdw.us].GETNEXT3.Task T
	INNER JOIN [phoenixdw.us].GETNEXT3.Activity A ON A.TaskID = T.TaskID
	INNER JOIN [phoenixdw.us].GETNEXT3.ActivityType ATT ON ATT.ActivityTypeID = A.ActivityTypeID
	LEFT JOIN [phoenixdw.us].GETNEXT3.ActivityTypeGroup ATG ON ATT.ActivityTypeGroupID = ATG.ActivityTypeGroupID
	INNER JOIN [phoenixdw.us].SOURCE.Source S ON A._SourceID = S.SourceID
	INNER JOIN [phoenixdw.us].SOURCE.MediaChannel MC ON S.MediaChannelID = MC.MediaChannelID
	LEFT JOIN [phoenixdw.us].REPORTING.SourceVendor SV ON S.SourceID = SV.SourceID
	--LEFT JOIN [phoenixdw.us].VIEWS.vw_AS_TVStation TV ON S.SourceID = TV.SourceID
	--LEFT JOIN [phoenixdw.us].VIEWS.vw_AS_CallTransferVendor CT ON S.SourceID = CT.SourceID
	INNER JOIN [phoenixdw.us].dbo.Product P ON A._ProductTypeID = P.ProductTypeID
	INNER JOIN [phoenixdw.us].dbo.Brand B ON P.BrandID = B.BrandID
	INNER JOIN [phoenixdw.us].dbo.ProductTypeGroup PTG ON P.ProductTypeGroupID = PTG.ProductTypeGroupID

	-- Run-off tasks
	LEFT JOIN
	(
		SELECT
			TaskID,
			MAX(CASE
				WHEN S.MediaChannelID in (1, 3, 12, 13, 16, 17, 18, 23, 24, 27, 34)			-- select * from phoenix.source.mediachannel where mediachannelid in  (1, 3, 12, 13, 16, 17, 18, 23, 24, 27, 34)
					--and (oc.OnlineChannelGroupID is null or oc.OnlineChannelGroupID in (2, 3, 11))		-- 2= Paid Brand, 3= SEO or 11= Direct
					THEN 'Inbound - TV'
				WHEN S.MediaChannelID in (35, 36) or (S.SourceID between 5052 and 5058)  then 'Data leads'
				WHEN S.MediaChannelID in (39, 40) then 'Inbound - Call Transfers'
				WHEN S.MediaChannelID in (9, 10) THEN 'Survey Leads'
				WHEN S.MediaChannelID in (16, 17, 18, 34) THEN 'Survey Leads'
				WHEN S.MediaChannelID in (8, 33) THEN 'DM'
				WHEN S.MediaChannelID in (3, 16, 17, 18, 23, 24, 29, 34) THEN '3rd party live leads'
				WHEN S.MediaChannelID in (11) and S.MktingCampaignName like '%Re-harvest%' THEN S.MktingCampaignName
				WHEN S.MediaChannelID in (30, 31, 32) THEN S.MktingCampaignName
				WHEN S.MediaChannelID in (11) and S.MktingCampaignName like 'DM/TM%' THEN 'DM'
				ELSE 'Inbound'						-- other
			END) AS ActivityGroup,
			MAX(CASE WHEN MC.LeadTypeCode = 'I' THEN 1 ELSE 0 END) AS isInboundCallActivity

		FROM [phoenixdw.us].dbo.ClientLead CL 
		INNER JOIN [phoenixdw.us].SOURCE.Source S ON CL.SourceID = S.SourceID
		INNER JOIN [phoenixdw.us].SOURCE.MediaChannel MC ON S.MediaChannelID = MC.MediaChannelID
		INNER JOIN 
		(
			SELECT *
			FROM
			(
				SELECT
					T.TaskID,
					T.dtmCreated,
					EntityID,
					ROW_NUMBER() OVER(PARTITION BY A.TaskID ORDER BY A.ActivityID) AS ActivityOrder,
					A.ActivityTypeID,
					isInitialActivity

				FROM [phoenixdw.us].GETNEXT3.Task T
				INNER JOIN [phoenixdw.us].GETNEXT3.Activity A ON A.TaskID = T.TaskID
				INNER JOIN [phoenixdw.us].GETNEXT3.ActivityType ATT ON ATT.ActivityTypeID = A.ActivityTypeID
				WHERE EntityIDTypeID = 1
			) T
			WHERE ActivityOrder = 1
			AND isInitialActivity = 0
			AND ActivityTypeID <> 48 -- Removing abandoned calls
			AND dtmCreated <= '2017-06-19 00:00:00' -- Before the official launch of GETNEXT
		) T
		ON CL.ClientLeadID = T.EntityID

		GROUP BY TaskID
	) RT
	ON T.TaskID = RT.TaskID
) A
WHERE ActivityOrder = 1